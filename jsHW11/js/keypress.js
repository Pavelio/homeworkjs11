function keyPress(event) {
    let theButton = document.getElementsByClassName('btn');
    for (let i = 0; i < theButton.length; i++) {
        theButton[i].style.background = "#33333a";
    }

    switch (event.which) {
        case 13:
            document.getElementById('btn1').style.background = 'blue';
            break;
        case 115:
            document.getElementById('btn2').style.background = 'blue';
            break;
        case 101:
            document.getElementById('btn3').style.background = 'blue';
            break;
        case 111:
            document.getElementById('btn4').style.background = 'blue';
            break;
        case 110:
            document.getElementById('btn5').style.background = 'blue';
            break;
        case 108:
            document.getElementById('btn6').style.background = 'blue';
            break;
        case 122:
            document.getElementById('btn7').style.background = 'blue';
            break;
    }
}
addEventListener("keypress", keyPress);